$fn=50;

module rounded_cube(dimensions, radius, center) {
    length = dimensions[0];
    width = dimensions[1];
    height = dimensions[2];
    linear_extrude(height, center=center) {
        offset(r=radius){
            offset(delta=-radius){
                square([length, width], center=center);
            }
        }
    }
}

width = 170;
height = 100;
depth = 1.6;
radius = 5;

hole_radius = 4;
hole_spacing = 8;

jigger1_radius = 34 / 2;
jigger2_radius = 39 / 2;
shaker_radius = 75 / 2;

// bottom face
difference(){
    rounded_cube([width, height, depth], radius, false);
    
    union(){
        // bottom left
        translate([hole_spacing, hole_spacing, 0])
        cylinder(r=hole_radius, h=10, center=true);
        // top left
        translate([hole_spacing, height - hole_spacing, 0])
        cylinder(r=hole_radius, h=10, center=true);
        // bottom right
        translate([width - hole_spacing, hole_spacing, 0])
        cylinder(r=hole_radius, h=10, center=true);
        // top right
        translate([width - hole_spacing, height - hole_spacing, 0])
        cylinder(r=hole_radius, h=10, center=true);
    }
}

// top face
difference(){
    rounded_cube([width, height, depth], radius, false);
    
    union(){
        // bottom left
        translate([hole_spacing, hole_spacing, 0])
        cylinder(r=hole_radius, h=10, center=true);
        // top left
        translate([hole_spacing, height - hole_spacing, 0])
        cylinder(r=hole_radius, h=10, center=true);
        // bottom right
        translate([width - hole_spacing, hole_spacing, 0])
        cylinder(r=hole_radius, h=10, center=true);
        // top right
        translate([width - hole_spacing, height - hole_spacing, 0])
        cylinder(r=hole_radius, h=10, center=true);

        // jigger 1
        translate([width / 2 + 45, height / 2 + 22.5, 0])
        cylinder(r=jigger1_radius, h=10, center=true);
        // jigger 2
        translate([width / 2 + 45, height / 2 - 22.5, 0])
        cylinder(r=jigger2_radius, h=10, center=true);
        // shaker
        translate([width / 2 - 30, height / 2, 0])
        cylinder(r=shaker_radius, h=10, center=true);
    }
}
