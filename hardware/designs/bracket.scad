$fn=50;

module rounded_cube(dimensions, radius, center) {
    length = dimensions[0];
    width = dimensions[1];
    height = dimensions[2];
    linear_extrude(height, center=center) {
        offset(r=radius){
            offset(delta=-radius){
                square([length, width], center=center);
            }
        }
    }
}

width = 50;
height = 50;
depth = 5;
radius = 5;

union(){
    rounded_cube([width, height, depth], radius, false);
    
    rotate([90, 0, 0]) {
        translate([0, 0, -depth])
        rounded_cube([width, height/2.5, depth], radius, false);
    }
}
