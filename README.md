# tikibAR

"Bringing the tiki experience to your device" - [Live Demo](https://nbtech.to/tikibAR)

An Augmented Reality (AR) tikibar experience for iOS and Instagram that can be web-hosted. 

<img src="website.png" width="800px"> 

- [tikibAR](#tikibar)
  - [Background](#background)
  - [About The Project](#about-the-project)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [tikibar](#tikibar-1)
    - [Instagram Filter](#instagram-filter)
    - [Tiki Masks](#tiki-masks)
  - [Design Notes](#design-notes)
  - [Contributing](#contributing)


## Background

This year, I got invited to a tiki-themed Halloween party that had a costume contest and as I have been getting into tiki bars a lot more recently I felt it was pertinent to make my costume tiki-themed as well. In the weeks leading up to the party, I came across a video of someone at a conference who had made an Instagram AR filter that tracked their clothing and added a small experience around them. This gave me the idea to make an Instagram filter that makes it look like I was standing in a tiki bar. I spent some time (way more than I care to admit) trying to get this working in Instagram's [Meta Spark Studio](https://spark.meta.com) but couldn't get around the file size limitations (40MB for the overall package and 4MB for each 3D model in the package). 

While working on the Instagram filter, I came across a great source for textured 3D models, [Sketchfab.com](https://sketchfab.com/feed). I found all of the 3D models that I used in this project from this site and a huge thanks to all the great designers on there. 

After giving up on the Instagram route, I switched to trying [Apple's AR development experience](https://developer.apple.com/augmented-reality/tools/) to see if it could work and fell in love. At the time of making this, Apple has two main products for creating AR experiences; [Reality Composer](https://apps.apple.com/app/reality-composer/id1462358802/) (an iOS app for creating simple experiences) and [Reality Composer Pro](https://apps.apple.com/us/app/xcode/id497799835?mt=12) (an extension of the Xcode desktop app for creating more complex experiences to be used in apps). I couldn't initially figure out how to get Reality Composer Pro working on my computer (later found out the latest version of Xcode had a bug where the app integration wasn't showing up properly) so I instead switched to using Reality Composer. 

The whole development experience was so easy. Drag-and-drop assets (3D models, music, pictures, etc.) into the world, add behaviors (like continus animations, background music, and location-based triggers), and then view the experience directly on device. Being able to develop directly on the device it was going to be experienced on was a game-changer. No waiting around for things to export and transfer over to the device, when you find an issue in the experience you can update it in place, and lastly, what you see is what you get so there are no translation issues from device to device.

After building the full iOS experience, I later thought about just making some simple tiki masks too which I integrated into an Instagram filter and created separate iOS AR files for. Once I had all these different experiences, I needed a way to put them together so I created the website so I could easily share it with people. Once I started hosting it, I printed the "Trader Nate's" [menu](software/exports/menu.pdf) out on small pieces of paper with the QR code on the back to the website, "laminated" them with clear packaging tape, and handed them out at the party. 

## About The Project

In this project I have created a complex [tikibar](website/tb_TikiBar.reality) iOS AR experience, three simple iOS AR masks ([1](website/tb_TikiMask_1.reality), [2](website/tb_TikiMask_2.reality), [3](website/tb_TikiMask_3.reality)), an Instagram [filter](https://www.instagram.com/ar/3297450287138302/?ch=YzQwYmE2ZjkzOWUyMTdjYWE3NjgyZmZjNTRhZjgzZGM%3D) with 3 unique tiki masks, and a [website](https://nbtech.to/tikibAR) to host the whole project (no promises on this staying live forever but feel free to reach out if it stops working and I'll try and fix it when I get a second).

There are two easter eggs in the tikibar experience, one with a sound effect and animation, the other gets triggered when your camera enters a certain location and continues for 30 seconds. If you want to check if you're right or want to cheat, feel free to download the project files from the designs folder and open them with the Reality Composer app. Then go to the behaviors section and find the easter egg behaviors and it will show you what to expect.

## Getting Started

To get started, you can open any of the files directly or visit [live demo](https://nbtech.to/tikibAR). 

### Prerequisites
To get the AR experience to open seamlessly on iOS, you have to open it in Safari. These experiences seem to work great on the latest mobile hardware. Object occlusion in the iOS AR experiences works best on devices with LiDAR (like the latest Pro devices). 

### tikibar
<img src="software/website/tb_tikibar.png" height="800px" width="800px">

This is the full tiki bar iOS AR experience, what I spent most of my time working on. It is intended for me to stand, dressed up as a tiki bartender, behind the bar and "serve" you drinks. 

When opening the experience, it will load a scaled down version of the model. Once you see this, point your device at the ground so it can align the ground of the model with the ground in your space. It should scale up the model to be life-size and start you standing in front of the bar. At this point, feel free to walk around! (Be sure to look for the easter eggs!)

### Instagram Filter
This Instagram filter puts tiki masks on people in your photo. 

When you first open the Instagram filter it will ask you if you trust the developer, you can just press "Continue". This is because I didn't go through the process of publishing this filter permanently and just have a developer link to the filter. This developer link should stay active forever but only allows up to 50 unique visitors each day. Once it opens, you can put up to 3 unique tiki masks over 3 faces. They always show up in the same order so if you want to put specific masks on specific faces, just change the order in which the camera sees each face.

### Tiki Masks
These use the iOS AR experience to track faces and overlay the tiki mask, similar to the Instagram filter, on faces in the camera. 

<img src="software/website/tb_mask_1.png" width="250"> <img src="software/website/tb_mask_2.png" width="250"> <img src="software/website/tb_mask_3.png" width="250px">

## Design Notes
* One big problem, mentioned in the background, is the issue of file sizes. I started by importing simple models to the project but tried to export and preview them and was quickly hit with the `Effect file is too big. Reduce each .arfx file to 4MB or less.` and `Effect file is too big. Reduce the .arexport file to 40MB or less.` errors. I played with this for a while but even while adjusting project compression resolution and quality I couldn't get enough 3D models into this to make the full experience. Retrospectively, these file size limitations make a lot of sense because when people load the full tikibar experience on iOS, it can take a couple seconds, sometimes even a minute or two, because the file is so large. As much as I would have enjoyed doing the entire thing in the Instagram filter, I'm glad I went the route I did because it led me to working with Apple's AR development experience. 
* If you look really carefully, you'll notice there are a few issues with some of the tiki masks. Most notably, some of the pieces of wood show up as inverted (the texture is pointing inward on the wood). I believe this is an artifact of the conversion process that happens when downloading the files from [Sketchfab.com](https://sketchfab.com/feed). I tried spending some time fixing these artifacts (again way more time than I care to admit) but I haven't done any texture-based 3D modeling before and was just completely out of my element. If you, or someone you know, is good at 3D modeling with textures, please reach out! I would love to learn more about it!
* Like I mentioned above, I am not a texture-based 3D modeler. I can make great 3D models and I am great with image editors, but putting those two things together I just have no clue where to even start. Ideally, I would have created all the models from scratch, or found a way to simplify the geometry of the models I found online so that I'm just loading one static model for the environment and then a few small models for the animated parts. This would have made the overall project size smaller while still creating the same great experience. I would love to revisit this project once I have more experience with texture-based 3D modeling. 
* Somewhere along the line, I thought it would be cool to make a "window" (similar to the windows in Disney's Trader Sam's tiki bars) where you can see volcano's errupting in the distance. The easy way to do this would be to import a video (like [this](https://www.youtube.com/watch?v=92BL9ZzxCSk) one) and track it to a physical photo frame. The photo frame tracking is the easy part because that is already built into Apple's AR experience but sadly there doesn't seem to exist a way to include a video without creating a full-on app with Reality Composer Pro. I would love to add this to the tikibAR experience some day if Apple makes it easier to import videos. 
* I touched on this a little but the loading times are a bit rough when loading from the website. I have served the file from an all flash file server on the same network as the test device and it can take up to 25 seconds just to download the file. This is compounded when loading from slow cellular networks (not to mention using a lot of people's cellular data). I was able to work around this at the party that I went to by Airdropping the main experience file to people (assuming they had available storage on their phone) or just give my phone to them if they had anything that wasn't an iOS device. I would I would like to rebuild this someday using smaller, less complex 3D models. This would decrease the file sizes and therefore loading times. 
* If you choose to host the website files yourself, the `.reality` files require being served with a custom MIME type `model/vnd.usdz+zip`. If you don't add this, it won't add the AR overlay button that allows for quickly viewing the file. More information can be found [here](https://webkit.org/blog/8421/viewing-augmented-reality-assets-in-safari-for-ios/). Also, make sure to unzip the `software/exports/TikiBar.reality` file into the website folder as `tb_TikiBar.reality`. You can use this command to recompile the parts and then unzip it: `zip -s0 TikiBar.reality.zip --out output.zip && ditto -xk output.zip ./` 
* You might notice a few extra files like the "holder" and "bracket", these are all part of the physical costume I made to go along with this project. The "holder" is for holding a cocktail shaker and two jiggers onto a bamboo cutting board that I hung around my neck. The "bracket" was attached to the bottom of the bamboo cutting board so the bracket would fit in my belt and stabilize the whole thing so I don't have to hold it the whole night. My brother provided me with a custom coconut mug (made of a real coconut) that he had been working on for a couple months. I also walked around with liquid-activated glow cubes that I handed out to people. I also bought a couple of airplane bottles of different liquors that I carried around to make it look like I had a selection of beverages to make drinks out of.

## Contributing
Feel free to contribute to this project. I welcome you to open issues, make pull requests, or just fork it and add your own features!
